﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SuperCD : MonoBehaviour {

	//private GameObject[] triangleList;
	private int triangleCount;

	private struct BoundingCircle {
		public Vector3 center;
		public float radius;
	}

	private struct AABB {
		public Vector3 min;
		public Vector3 max;
	}

	private struct OOBB {
		//public Vector3 a, b, c, d;
		public Vector2 center;
		public Vector2 localXAxis;
		public Vector2 localYAxis;
		public float halfWidth;
		public float halfHeight;
	}

	private struct MinkowskyHull {
		public Vector3[] minkowskiHullVertices;
	}

	private struct triangleStruct {
		public GameObject obj;
		public BoundingCircle bCircle;
		public AABB aaBox;
		public OOBB ooBox;
		public MinkowskyHull[] minkowskyHulls;
	}

	private triangleStruct[] triangleList;

	void createTriangles() {
		float screenWidth = Camera.main.orthographicSize * Camera.main.aspect;
		float screenHeight = Camera.main.orthographicSize;
		float randomPosX;
		float randomPosY;
		float randomRotZ;

		Vector3[] vertices = new Vector3[3];
		int[] indices = new int[3];

		for (int i=0; i<triangleCount; i++) {
			randomPosX = Random.Range (screenWidth, -screenWidth);
			randomPosY = Random.Range (screenHeight, -screenHeight);
			randomRotZ = Random.Range (0.0f, 359.99f);

			vertices [0] = new Vector3 (-1.0f, -1.0f, 0.0f);
			vertices [1] = new Vector3 (-1.0f, +1.0f, 0.0f);
			vertices [2] = new Vector3 (0.0f, 1.0f, 0.0f);
			indices [0] = 0;
			indices [1] = 1;
			indices [2] = 2;

			triangleList[i].obj = new GameObject("Triangle");

			MeshFilter mf = (MeshFilter)triangleList[i].obj.AddComponent(typeof(MeshFilter));
			
			Mesh m = new Mesh();
			m.vertices = vertices;
			m.SetIndices(indices, MeshTopology.Triangles, 0);
			m.RecalculateBounds();

			mf.mesh = m;
			
			MeshRenderer renderer = triangleList[i].obj.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
			renderer.material.shader = Shader.Find ("Custom/FlatColor");
			//renderer.material.color = new Color(0.8f, 0.1f, 0.0f, 1.0f); 
			renderer.material.SetColor("_Color", new Color(1.0f, 1.0f, 1.0f, 1.0f)); 
			triangleList[i].obj.transform.position = new Vector3 (randomPosX, randomPosY, 0.0f);
			triangleList[i].obj.transform.Rotate(0.0f, 0.0f, randomRotZ);
			//triangleList[i].obj.transform.Rotate(0.0f, 0.0f, 0.0f);
		}
	}

	void drawBox(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4) {
		Vector3[] vertices = new Vector3[4];
		int[] indices = new int[6];
		
		for (int i=0; i<4; i++) {
			vertices [0] = new Vector3 (p1.x, p1.y, 0.0f);
			vertices [1] = new Vector3 (p2.x, p2.y, 0.0f);
			vertices [2] = new Vector3 (p3.x, p3.y, 0.0f);
			vertices [3] = new Vector3 (p4.x, p4.y, 0.0f);
			indices [0] = 0;
			indices [1] = 3;
			indices [2] = 1;
			indices [3] = 1;
			indices [4] = 3;
			indices [5] = 2;
			
			triangleList[i].obj = new GameObject("Triangle");
			
			MeshFilter mf = (MeshFilter)triangleList[i].obj.AddComponent(typeof(MeshFilter));
			
			Mesh m = new Mesh();
			m.vertices = vertices;
			m.SetIndices(indices, MeshTopology.Triangles, 0);
			m.RecalculateBounds();
			
			mf.mesh = m;
			
			MeshRenderer renderer = triangleList[i].obj.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
			renderer.material.shader = Shader.Find ("Diffuse");
			//renderer.material.color = new Color(0.8f, 0.1f, 0.0f, 1.0f); 
			renderer.material.SetColor("_Color", new Color(1.0f, 1.0f, 0.0f, 1.0f)); 
			//triangleList[i].obj.transform.position = new Vector3 (randomPosX, randomPosY, 0.0f);
			//triangleList[i].obj.transform.Rotate(0.0f, 0.0f, randomRotZ);
			triangleList[i].obj.transform.Rotate(0.0f, 0.0f, 0.0f);
		}
	}

	List<Vector3> computeMinkowskiDiff(triangleStruct a, triangleStruct b) {
		List<Vector3> minkovskiDiff = new List<Vector3>();
		Vector3[] verticesA = a.obj.GetComponent<MeshFilter> ().sharedMesh.vertices;
		Vector3[] verticesB = b.obj.GetComponent<MeshFilter> ().sharedMesh.vertices;

		for (int i=0; i < verticesA.Length; i++) {
			verticesA[i] = a.obj.transform.TransformPoint(verticesA[i]);
		}

		for (int i=0; i < verticesB.Length; i++) {
			verticesB[i] = b.obj.transform.TransformPoint(verticesB[i]);
		}

		for (int i=0; i < verticesA.Length; i++) {
			for (int j=0; j < verticesB.Length; j++) {
				Vector3 minkovskiPoint = verticesA[i] - verticesB[j];
				minkovskiDiff.Add(minkovskiPoint);
			}
		}

		return minkovskiDiff;
	}

	int pointLocation(Vector3 A, Vector3 B, Vector3 P) {
		float cp1 = (B.x-A.x)*(P.y-A.y) - (B.y-A.y)*(P.x-A.x);
		return (cp1>0.0f)?1:-1;
	}

	float distance(Vector3 A, Vector3 B, Vector3 C) {
		float ABx = B.x-A.x;
		float ABy = B.y-A.y;
		float num = ABx*(A.y-C.y)-ABy*(A.x-C.x);
		if (num < 0.0f) {
			num = -num;
		}
		return num;
	}

	public void hullSet(Vector3 A, Vector3 B, List<Vector3> set, List<Vector3> hull) {
		int insertPosition = hull.IndexOf(B);
		if (set.Count == 0) {
			return;
		}
		if (set.Count == 1) {
			Vector3 p = new Vector3(set[0].x, set[0].y, set[0].z);
			set.Remove(p);
			hull.Insert(insertPosition,p);
			return;
		}
		float dist = -9999999.99f;
		int furthestPoint = -1;
		for (int i = 0; i < set.Count; i++) {
			Vector3 p = new Vector3(set[i].x, set[i].y, set[i].z);
			float squaredDistance  = distance(A,B,p);
			if (squaredDistance > dist) {
				dist = squaredDistance;
				furthestPoint = i;
			}
		}
		Vector3 P = new Vector3(set[furthestPoint].x, set[furthestPoint].y, set[furthestPoint].z);;
		set.RemoveAt(furthestPoint);
		hull.Insert(insertPosition,P);
		    
		// Determine who's to the left of AP
		List<Vector3> leftSetAP = new List<Vector3>();
		for (int i = 0; i < set.Count; i++) {
			Vector3 M = new Vector3(set[i].x, set[i].y, set[i].z);
			if (pointLocation(A,P,M)==1) {
				//set.remove(M);
				leftSetAP.Add(M);
			}
		}
		    
		// Determine who's to the left of PB
		List<Vector3> leftSetPB = new List<Vector3>();
		for (int i = 0; i < set.Count; i++) {
			Vector3 M = new Vector3(set[i].x, set[i].y, set[i].z);
			if (pointLocation(P,B,M)==1) {
				//set.remove(M);
				leftSetPB.Add(M);
			}
		}
		hullSet(A,P,leftSetAP,hull);
		hullSet(P,B,leftSetPB,hull);
	}

	MinkowskyHull computeConvexHull(List<Vector3> minkowskiDiff) {
		MinkowskyHull minkovskiHull = new MinkowskyHull();
		List<Vector3> convexHull = new List<Vector3>();
		if (minkowskiDiff.Count < 3) {
			minkowskiDiff.CopyTo(minkovskiHull.minkowskiHullVertices);
			return minkovskiHull;
		}

		// find extremals
		int minPoint = -1;
		int	maxPoint = -1;
		float minX = 9999999.99f;
		float maxX = -9999999.99f;
		for (int i = 0; i < minkowskiDiff.Count; i++) {
			if (minkowskiDiff[i].x < minX) {
				minX = minkowskiDiff[i].x;
			    minPoint = i;
			} 
			if (minkowskiDiff[i].x > maxX) {
				maxX = minkowskiDiff[i].x;
			    maxPoint = i;       
			}
		}
		Vector3 A = minkowskiDiff[minPoint];
		Vector3 B = minkowskiDiff[maxPoint];
		convexHull.Add(A);
		convexHull.Add(B);
		minkowskiDiff.Remove(A);
		minkowskiDiff.Remove(B);
		    
		List<Vector3> leftSet = new List<Vector3>();
		List<Vector3> rightSet = new List<Vector3>();
		    
		for (int i = 0; i < minkowskiDiff.Count; i++) {
		   	Vector3 p = minkowskiDiff[i];
		    if (pointLocation(A,B,p) == -1)
				leftSet.Add(p);
		    else
		        rightSet.Add(p);
		}
		hullSet(A,B,rightSet,convexHull);
		hullSet(B,A,leftSet,convexHull);

		minkovskiHull.minkowskiHullVertices = new Vector3[convexHull.Count];
		convexHull.CopyTo (minkovskiHull.minkowskiHullVertices);

		return minkovskiHull;
	}

	void recalculateBoundingPrimitives() {
		Vector3[] vertices = new Vector3[3];
		Mesh mesh;
		MeshRenderer renderer;
		float dist;
		float angle;
		Vector3[] edges = new Vector3[3];
		Vector3 xAxis = new Vector3 (1.0f, 0.0f, 0.0f);
		float minX, maxX, minY, maxY;
		float minArea = 999999.99f;
		Vector2 e1, e2, p1, p2, p3, p4, pt1, pt2, pt3, pt4;
		pt1 = pt2 = pt3 = pt4 = new Vector2 (0.0f, 0.0f);
		List<Vector3> minkowskiDiff = new List<Vector3>();

		for (int i=0; i<triangleCount; i++) {
			mesh = triangleList[i].obj.GetComponent<MeshFilter>().sharedMesh;
			renderer = triangleList[i].obj.GetComponent<MeshRenderer>();
			vertices = mesh.vertices;
			triangleList[i].bCircle.center = renderer.bounds.center;
			triangleList[i].bCircle.radius = 0.0f;

			for (int j=0; j<3; j++) {
				//dist = (vertices[j] - mesh.bounds.center).sqrMagnitude;
				dist = (vertices[j] - mesh.bounds.center).magnitude;
				if (dist > triangleList[i].bCircle.radius) {
					triangleList[i].bCircle.radius = dist;
				}
			}
			triangleList[i].aaBox.min = renderer.bounds.min;
			triangleList[i].aaBox.max = renderer.bounds.max;

			//Vector2 point2 = new Vector2(triangleList[i].aaBox.max.x, triangleList[i].aaBox.min.y); 
			//Vector2 point4 = new Vector2(triangleList[i].aaBox.min.x, triangleList[i].aaBox.max.y);

			//drawBox(triangleList[i].aaBox.min, point2, triangleList[i].aaBox.max, point4);

			edges[0] = (triangleList[i].obj.transform.TransformPoint(vertices[1]) - triangleList[i].obj.transform.TransformPoint(vertices[0])).normalized;
			edges[1] = (triangleList[i].obj.transform.TransformPoint(vertices[2]) - triangleList[i].obj.transform.TransformPoint(vertices[1])).normalized;
			edges[2] = (triangleList[i].obj.transform.TransformPoint(vertices[0]) - triangleList[i].obj.transform.TransformPoint(vertices[2])).normalized;

			for (int j=0; j<3; j++) {
				minX = minY = 999999.99f;
				maxX = maxY = -999999.99f;
				minArea = 999999.99f;
				angle = Mathf.Acos(Vector3.Dot (edges[j], xAxis));
				angle = angle * (180/Mathf.PI);
				if (edges[j].y < 0.0f) {
					angle = 360.0f - angle;
				}
				triangleList[i].obj.transform.Rotate(0.0f, 0.0f, -angle);
				for (int k=0; k<3; k++) {
					if (triangleList[i].obj.transform.TransformPoint(vertices[k]).x < minX) {
						minX = triangleList[i].obj.transform.TransformPoint(vertices[k]).x;
					} 
					if (triangleList[i].obj.transform.TransformPoint(vertices[k]).x >= maxX) {
						maxX = triangleList[i].obj.transform.TransformPoint(vertices[k]).x;
					} 
					if (triangleList[i].obj.transform.TransformPoint(vertices[k]).y < minY) {
						minY = triangleList[i].obj.transform.TransformPoint(vertices[k]).y;
					} 
					if (triangleList[i].obj.transform.TransformPoint(vertices[k]).y >= maxY) {
						maxY = triangleList[i].obj.transform.TransformPoint(vertices[k]).y;
					} 
				}

				//triangleList[i].obj.transform.Rotate(0.0f, 0.0f, angle);
				//triangleList[i].obj.transform.Rotate(0.0f, 0.0f, originalRotZ);

				p1 = new Vector2(minX, minY);
				p2 = new Vector2(maxX, minY);
				p3 = new Vector2(maxX, maxY);
				p4 = new Vector2(minX, maxY);

				p1 = triangleList[i].obj.transform.InverseTransformPoint (p1);
				p2 = triangleList[i].obj.transform.InverseTransformPoint (p2);
				p3 = triangleList[i].obj.transform.InverseTransformPoint (p3);
				p4 = triangleList[i].obj.transform.InverseTransformPoint (p4);
				triangleList[i].obj.transform.Rotate(0.0f, 0.0f, angle);

				e1 = p2 - p1;
				e2 = p3 - p2;

				if (e1.sqrMagnitude * e2.sqrMagnitude < minArea) {
					minArea = e1.sqrMagnitude * e2.sqrMagnitude;
					pt1 = triangleList[i].obj.transform.TransformPoint (p1);
					pt2 = triangleList[i].obj.transform.TransformPoint (p2);
					pt3 = triangleList[i].obj.transform.TransformPoint (p3);
					pt4 = triangleList[i].obj.transform.TransformPoint (p4);
				}
				//Debug.Log (p1 + ", " + p2 + ", " + p3 + ", " + p4 + ", " + minArea);
				//drawBox(triangleList[i].ooBox.a, triangleList[i].ooBox.b, triangleList[i].ooBox.c, triangleList[i].ooBox.d);
				//Debug.Log(triangleList[i].ooBox.a + ", " + triangleList[i].ooBox.b + ", " + triangleList[i].ooBox.c + ", " + triangleList[i].ooBox.d);
				//p1 = triangleList[i].obj.transform.TransformPoint(vertices[0]);
				//p2 = triangleList[i].obj.transform.TransformPoint(vertices[1]);
				//p3 = triangleList[i].obj.transform.TransformPoint(vertices[2]);
				//Debug.Log (p1 + "; " + p2 + "; " + p3);
			}

			triangleList[i].ooBox.localXAxis = pt2 - pt1;
			triangleList[i].ooBox.localYAxis = pt3 - pt2;
			triangleList[i].ooBox.halfWidth = triangleList[i].ooBox.localXAxis.magnitude*0.5f;
			triangleList[i].ooBox.halfHeight = triangleList[i].ooBox.localYAxis.magnitude*0.5f;
			triangleList[i].ooBox.localXAxis = triangleList[i].ooBox.localXAxis.normalized;
			triangleList[i].ooBox.localYAxis = triangleList[i].ooBox.localYAxis.normalized;
			triangleList[i].ooBox.center.x = (pt4.x + pt2.x) * 0.5f;
			triangleList[i].ooBox.center.y = (pt4.y + pt2.y) * 0.5f;
			//drawBox(triangleList[i].ooBox.a, triangleList[i].ooBox.b, triangleList[i].ooBox.c, triangleList[i].ooBox.d);
			//Debug.Log(triangleList[i].ooBox.a + ", " + triangleList[i].ooBox.b + ", " + triangleList[i].ooBox.c + ", " + triangleList[i].ooBox.d);
			//triangleList[i].obj.transform.Rotate(0.0f, 0.0f, -angle); 
		
			triangleList[i].minkowskyHulls = new MinkowskyHull[triangleCount];

			for (int j=0; j<triangleCount; j++) {
				if(i==j) {
					continue;
				}
				minkowskiDiff.Clear();
				minkowskiDiff = computeMinkowskiDiff(triangleList[i], triangleList[j]);
				MinkowskyHull hull = computeConvexHull(minkowskiDiff);

				triangleList[i].minkowskyHulls[j].minkowskiHullVertices = new Vector3[hull.minkowskiHullVertices.Length];
				triangleList[i].minkowskyHulls[j].minkowskiHullVertices = hull.minkowskiHullVertices;
			}
		}
	}

	bool testConvexPolyPoint(Vector3[] minkowskiHullVertices, Vector3 point) {
		int i;
		int nextI;
		int len; 
		Vector3 v1; 
		Vector3 v2; 
		//Vector3 edge;
		float x;

			// First translate the polygon so that `point` is the origin. Then, for each
			// edge, get the angle between two vectors: 1) the edge vector and 2) the
			// vector of the first vertex of the edge. If all of the angles are the same
			// sign (which is negative since they will be counter-clockwise) then the
			// point is inside the polygon; otherwise, the point is outside.
		for (i = 0, len = minkowskiHullVertices.Length; i < len; i++) {
			v1 = minkowskiHullVertices[i] - point;
			if(i+1 > len-1) {
				nextI = 0;
			} else {
				nextI = i+1;
			}
			v2 = minkowskiHullVertices[nextI] - point;
			//edge = v1 - v2;
			// Note that we could also do this by using the normal + dot product
			x = v1.x*v2.y - v1.y*v2.x;
			// If the point lies directly on an edge then count it as in the polygon
			if (x > 0.0f) { 
				//Debug.Log ("Not Colliding");
				return false;
			}
		}
		return true;
	}

	void testIntersection() {
		float magnitudeSquared;
		float radiusSquared;
		List<int> indexList = new List<int> ();
		List<int> tmpIndexList = new List<int> ();
		Vector3 minA;
		Vector3 maxA;
		Vector3 minB;
		Vector3 maxB;
		int indexA;
		int indexB;
		Vector2 t, aX, aY, bX, bY;
		float wA, wB, hA, hB;
		Vector2[] l = new Vector2[4];
		bool isColliding = false;

		// Sphere Bounding Volume Test
		for (int i=0; i<triangleCount; i++) {
			triangleList[i].obj.renderer.material.SetColor("_Color", new Color(1.0f, 1.0f, 1.0f, 1.0f));
			for (int j=0; j<triangleCount; j++) {
				if (i==j) {
					continue;
				}
				magnitudeSquared = (triangleList[i].bCircle.center - triangleList[j].bCircle.center).sqrMagnitude;
				radiusSquared = Mathf.Pow(triangleList[i].bCircle.radius + triangleList[j].bCircle.radius, 2);
				//magnitudeSquared = (triangleList[i].bCircle.center - triangleList[j].bCircle.center).magnitude;
				//radiusSquared = triangleList[i].bCircle.radius + triangleList[j].bCircle.radius;
				if (magnitudeSquared < radiusSquared) { 
					triangleList[i].obj.renderer.material.SetColor("_Color", new Color(1.0f, 0.0f, 0.0f, 1.0f));
					indexList.Add(i);
					break;
				}
			}
			//indexList.Add (i);
		}

		Debug.Log (indexList.Count);

		// AABB Test
		for (int i=0; i<indexList.Count; i++) {
			indexA = indexList[i];
			minA = triangleList[indexA].aaBox.min;
			maxA = triangleList[indexA].aaBox.max;
			for (int j=0; j<indexList.Count; j++) {
				if (i==j) {
					continue;
				}
				isColliding = false;

				indexB = indexList[j];
				minB = triangleList[indexB].aaBox.min;
				maxB = triangleList[indexB].aaBox.max;

				if (maxA.x < minB.x ||
				    maxA.y < minB.y ||
				    minA.x > maxB.x ||
				    minA.y > maxB.y) {
					continue;
				}

				isColliding = true;
				break;
			}

			if (isColliding == true) {
				triangleList[indexA].obj.renderer.material.SetColor("_Color", new Color(1.0f, 1.0f, 0.0f, 1.0f));
				tmpIndexList.Add(indexA);
			}
		}

		indexList = new List<int>(tmpIndexList);
		tmpIndexList.Clear ();
		Debug.Log (indexList.Count);

		// OOBB Test
		for (int i=0; i<indexList.Count; i++) {
			indexA = indexList[i];
			isColliding = false;
			for (int j=0; j<indexList.Count; j++) {
				if (i==j) {
					continue;
				}

				indexB = indexList[j];
				t = triangleList[indexB].ooBox.center - triangleList[indexA].ooBox.center;
				l[0] = triangleList[indexA].ooBox.localXAxis;
				l[1] = triangleList[indexA].ooBox.localYAxis;
				l[2] = triangleList[indexB].ooBox.localXAxis;
				l[3] = triangleList[indexB].ooBox.localYAxis;
				aX = triangleList[indexA].ooBox.localXAxis;
				aY = triangleList[indexA].ooBox.localYAxis;
				bX = triangleList[indexB].ooBox.localXAxis;
				bY = triangleList[indexB].ooBox.localYAxis;
				wA = triangleList[indexA].ooBox.halfWidth;
				hA = triangleList[indexA].ooBox.halfHeight;
				wB = triangleList[indexB].ooBox.halfWidth;
				hB = triangleList[indexB].ooBox.halfHeight;

				if(Mathf.Abs(Vector2.Dot(t,l[0])) > wA + Mathf.Abs(Vector2.Dot(wB*bX, aX)) + Mathf.Abs (Vector2.Dot(hB*bY, aX))) {
					continue;
				}

				if(Mathf.Abs(Vector2.Dot(t,l[1])) > hA + Mathf.Abs(Vector2.Dot(wB*bX, aY)) + Mathf.Abs (Vector2.Dot(hB*bY, aY))) {
					continue;
				}

				if(Mathf.Abs(Vector2.Dot(t,l[2])) > wB + Mathf.Abs(Vector2.Dot(wA*aX, bX)) + Mathf.Abs (Vector2.Dot(hA*aY, bX))) {
					continue;
				}

				if(Mathf.Abs(Vector2.Dot(t,l[3])) > hB + Mathf.Abs(Vector2.Dot(wA*aX, bY)) + Mathf.Abs (Vector2.Dot(hA*aY, bY))) {
					continue;
				}

				isColliding = true;
				break;

			}

			if(isColliding == true) {
				tmpIndexList.Add(indexA);
				triangleList[indexA].obj.renderer.material.SetColor("_Color", new Color(0.0f, 0.0f, 1.0f, 1.0f));
			}
		}

		indexList = new List<int>(tmpIndexList);
		tmpIndexList.Clear ();
		Debug.Log (indexList.Count);

		// Minkowski Sum Test
		for (int i=0; i<indexList.Count; i++) {
			indexA = indexList[i];
			for (int j=0; j<indexList.Count; j++) {
				if (i==j) {
					continue;
				}
				indexB = indexList[j];
				//bool test = testConvexPolyPoint(triangleList[indexA].minkowskyHulls[indexB].minkowskiHullVertices, new Vector3(0.0f, 0.0f, 0.0f));
				if (testConvexPolyPoint(triangleList[indexA].minkowskyHulls[indexB].minkowskiHullVertices, new Vector3(0.0f, 0.0f, 0.0f))) {
				//if(test) {
					triangleList[indexA].obj.renderer.material.SetColor("_Color", new Color(0.0f, 0.0f, 0.0f, 1.0f));
					break;
				}
			}
		}
	}

	// Use this for initialization
	void Start () {
		triangleCount = 300;
		//triangleList = new GameObject[triangleCount];
		triangleList = new triangleStruct[triangleCount];
		createTriangles ();
		recalculateBoundingPrimitives ();
		testIntersection ();
	}
	
	// Update is called once per frame
	void Update () {
		handleInput ();
		//recalculateBoundingPrimitives ();
		//testIntersection ();
	}

	void handleInput() {
		Vector3 position = new Vector3();
		Vector3 movement = new Vector3(0.0f, 0.0f, 0.0f);
		float speed = 0.5f;
		bool hasMoved = false;

		if (Input.GetKey (KeyCode.LeftArrow)) {
			movement = movement + new Vector3(-1.0f, 0.0f, 0.0f);
			hasMoved = true;
		}

		if (Input.GetKey (KeyCode.RightArrow)) {
			movement = movement + new Vector3(1.0f, 0.0f, 0.0f);
			hasMoved = true;
		}

		if (Input.GetKey (KeyCode.UpArrow)) {
			movement = movement + new Vector3(0.0f, 1.0f, 0.0f);
			hasMoved = true;
		}

		if (Input.GetKey (KeyCode.DownArrow)) {
			movement = movement + new Vector3(0.0f, -1.0f, 0.0f);
			hasMoved = true;
		}

		if (hasMoved) {
			position = triangleList[0].obj.transform.position + (movement * speed);
			triangleList [0].obj.transform.position = position;
			updateCollissionShapes(ref triangleList[0], movement*speed);
			testIntersection();
		}
	}

	void updateCollissionShapes(ref triangleStruct triangle, Vector3 movement) {
		triangle.bCircle.center += movement;
		triangle.aaBox.max += movement;
		triangle.aaBox.min += movement;
		triangle.ooBox.center += new Vector2 (movement.x, movement.y);

		List<Vector3> minkowskiDiff = new List<Vector3>();
		triangle.minkowskyHulls = new MinkowskyHull[triangleCount];
		int index = -1;

		for (int i=0; i<triangleCount; i++) {
			if(triangleList[i].Equals(triangle)) {
				index = i; 
				break;
			}
		}
		
		for (int j=0; j<triangleCount; j++) {
			if(j==index) {
				continue;
			}
			minkowskiDiff.Clear();
			minkowskiDiff = computeMinkowskiDiff(triangleList[index], triangleList[j]);
			MinkowskyHull hull = computeConvexHull(minkowskiDiff);
			
			triangleList[index].minkowskyHulls[j].minkowskiHullVertices = new Vector3[hull.minkowskiHullVertices.Length];
			triangleList[index].minkowskyHulls[j].minkowskiHullVertices = hull.minkowskiHullVertices;
		}

		for (int j=0; j<triangleCount; j++) {
			if(j==index) {
				continue;
			}
			minkowskiDiff.Clear();
			minkowskiDiff = computeMinkowskiDiff(triangleList[j], triangleList[index]);
			MinkowskyHull hull = computeConvexHull(minkowskiDiff);
			
			triangleList[j].minkowskyHulls[index].minkowskiHullVertices = new Vector3[hull.minkowskiHullVertices.Length];
			triangleList[j].minkowskyHulls[index].minkowskiHullVertices = hull.minkowskiHullVertices;
		}
	}
}
